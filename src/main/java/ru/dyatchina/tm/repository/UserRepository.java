package ru.dyatchina.tm.repository;

import ru.dyatchina.tm.entity.User;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class UserRepository {
    private final List<User> users = new ArrayList<>();
    private final AtomicLong idGenerator = new AtomicLong();

    public void save(User user) {
        user.setId(idGenerator.incrementAndGet());
        users.add(user);
    }

    public List<User> findAll() {
        return users;
    }

    public User findByLogin(String login) {
        for (User user : users) {
            if (user.getLogin().equals(login)) {
                return user;
            }
        }
        return null;
    }

    public void deleteAll() {
        users.clear();
    }
}
