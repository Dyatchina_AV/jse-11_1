package ru.dyatchina.tm.service;

import org.apache.commons.codec.digest.DigestUtils;
import ru.dyatchina.tm.entity.Role;
import ru.dyatchina.tm.entity.User;
import ru.dyatchina.tm.repository.UserRepository;

import java.util.List;

public class UserService {
    private final UserRepository repository;

    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public boolean createUser(String login, String password, Role role) {
        if (repository.findByLogin(login) != null) {
            return false;
        }
        User user = new User();
        user.setLogin(login);
        user.setPassword(DigestUtils.md5Hex(password));
        user.setRole(role);
        repository.save(user);
        return true;
    }

    public List<User> getAllUsers() {
        return repository.findAll();
    }

    public void deleteAllUsers() {
        repository.deleteAll();
    }
}
